import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarControlComponent } from './sidebar-control.component';

describe('SidebarControlComponent', () => {
  let component: SidebarControlComponent;
  let fixture: ComponentFixture<SidebarControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
