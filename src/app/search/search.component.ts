import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { CarService } from '../car.service';
import { Car } from '../car';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  columnDefs = [
    { headerName: 'Make', field: 'make', sortable: true},
    { headerName: 'Model', field: 'model', sortable: true},
    { headerName: 'Price', field: 'price', sortable: true}
  ];
  rowData$: Observable<Car[]>;

  constructor(private carService: CarService) { }

  ngOnInit() {
    this.getCars();
  }

  private getCars(): void {
    this.rowData$ = this.carService.getCars();
  }
}
