export class Car {
    make: string;
    model: string;
    price: number;
}
