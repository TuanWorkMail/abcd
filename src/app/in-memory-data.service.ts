import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Injectable } from '@angular/core';

import { Car } from './car';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    let cars: Car[] = [];
    const CARS: Car[] = [
      { make: 'Toyota', model: 'Celica', price: 35000 },
      { make: 'Ford', model: 'Mondeo', price: 32000 },
      { make: 'Porsche', model: 'Boxter', price: 72000 },
      { make: 'Tesla', model: 'Roadster', price: 200000 },
      { make: 'Mazda', model: 'Mazda 3', price: 22000 }
    ];
    for (let index = 0; index < (100000 / 5); index++) {
      cars.unshift(...CARS);
    }
    console.log('first 10 rows of the generated 100.000 rows of car dataset');
    console.log(cars.slice(0, 10));
    setTimeout(() => {
      console.log('clearing data after sent to UI to reduce memory usage');
      cars = [];
    }, 1000);
    return { cars };
  }
}
